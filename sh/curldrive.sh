#!/bin/bash
#
# Transfer a file from Google drive
#
# Tested on Ubuntu 18.10
# Copyright: Charles Deledalle, 2019.
#

usage()
{
    echo -e "Usage: $0 LINK DEST ACCESSTOKEN"
    sed '1,/^#$/d;/^#$/,$d;s/^# //' $0
    echo
    echo -e "  LINK\tGoogle Drive file link."
    echo -e "  DEST\t\tDestination filename."
    echo -e "  ACCESSTOKEN\tGoogle Token Credential."
    echo
    sed '1,/^#$/d;1,/^#$/d;/^#$/,$d;s/^# *//' $0
}

if [ "$1" == "--help" ] ; then
    usage
    exit 0
fi
if [ $# -ne 3 ] ; then
    usage
    exit 2
fi

fileid=`echo "$1" | sed 's|https://drive.google.com/open?id=||'`

curl -H "Authorization: Bearer $3" "https://www.googleapis.com/drive/v3/files/${fileid}?alt=media" -o "$2"
