#!/bin/bash
#
# Create a Gnome's launcher for Conda's jupyter-notebook.
#
# Tested on Ubuntu 18.10 with Conda 4.6.3.
# Copyright: Charles Deledalle, 2019.
#

defaultdir="$HOME/Documents/"
if [ "$1" == "--help" ] ; then
    echo -e "Usage: $0 [DIR]"
    sed '1,/^#$/d;/^#$/,$d;s/^# //' $0
    echo
    echo -e "  DIR\t\tDirectory where to start jupyter on (default=$defaultdir)."
    echo -e "  --help\tDisplay this message."
    echo
    sed '1,/^#$/d;1,/^#$/d;/^#$/,$d;s/^# *//' $0
    exit 0
fi
if [ $# -ge 1 ] ; then
    defaultdir="$1"
fi
if ! [ -d "$defaultdir" ] ; then
    echo "No such directory: $defaultdir"
    exit 2
fi
PS1='dummy' source $HOME/.bashrc; conda activate;
if ! command -v conda > /dev/null ; then
    echo "Could not find Anaconda." \
	 "Please check your installation and make sure" \
	 "it is loaded correctly from your .bashrc ." | fold -w 70
    exit 2
fi
base=$(conda info --base)
enableconda="PS1='dummy' source $HOME/.bashrc; conda activate"
startjupyter="jupyter-notebook --notebook-dir $defaultdir"
icon=$(find "$base" -name 'jupyter-icon-*.png' -print -quit)
if ! [ -d "$HOME/.local/share/applications/" ] ; then
       mkdir -p "~/.local/share/applications/"
fi
if ! [ -f "$icon" ] ; then
    echo "Could not find Jupyter-notebook icon." \
	 "Please make sure that jupyter is installed?" \
	 "Type: conda install jupyter" | fold -w 70
    exit 2
fi
echo -e "Generating launcher:"
echo -e "  Icon:\t\t\t $icon"
echo -e "  Conda:\t\t $base"
echo -e "  Default directory:\t $defaultdir"
cat <<EOF > "$HOME/.local/share/applications/jupyter.desktop"
[Desktop Entry]
Type=Application
Terminal=true
Version=1
Name=Jupyter
Comment=Jupyter
Exec=bash -c "$enableconda; $startjupyter"
Icon=$icon
EOF
